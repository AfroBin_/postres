$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    
    $('#preguntar').on('show.bs.modal', function(e){
        console.log('El modal contacto se está mostrando');

        $('#preguntarBtn').removeClass('btn-outline-success');
        $('#preguntarBtn').addClass('btn-primary');
        $('#preguntarBtn').prop('disabled', true);
        
    });
    $('#preguntar').on('shown.bs.modal', function(e){
        console.log('El modal contacto se mostró');
    });
    $('#preguntar').on('hide.bs.modal', function(e){
        console.log('El modal contacto se oculta');
    });
    $('#preguntar').on('hidden.bs.modal', function(e){
        console.log('El modal contacto se ocultó');
        $('#preguntarBtn').removeClass('btn-primary');
        $('#preguntarBtn').addClass('btn-outline-success');
        $('#preguntarBtn').prop('disabled', false);
    });
});